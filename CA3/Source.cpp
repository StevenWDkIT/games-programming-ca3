#include "Item.h"
#include "Room.h"
#include "Parser.h"
#include "Random.h"
#include <iostream>
#include <ctime>
#include <algorithm>
#include <cctype>

using std::vector;
using std::ofstream;
using std::ifstream;
using std::string;
using std::cin;
using std::endl;
using std::cout;
using std::time;

void printIntro();
void hangmanPuzzle(Player& p);
void printPlayerDetails(Player& p);
int main()
{
	// File to read in from
	ifstream inFileRooms("Rooms.txt");
	ifstream inFileEnemy("Enemy.txt");
	ifstream inFileRoomsSaved("RoomSave.txt");
	ifstream inFilePlayerSaved("PlayerSave.txt");
	ifstream inFilePlayer("Player.txt");

	// Vector to hold all rooms
	vector<Room> rooms;
	vector<Enemy> enemies;

	// Number of rooms to load in
	int roomsNo = 0;
	int enemyNo = 0;
	string garbage;
	
	inFileRooms >> garbage >> roomsNo;
	inFileEnemy >> garbage >> enemyNo;
	
	// Load rooms
	for (int i = 0; i < roomsNo; ++i)
	{
		Room temp;
		
		if (inFileRoomsSaved)
		{
			temp.load(inFileRoomsSaved);
		}
		else
		{
			temp.load(inFileRooms);
		}
		rooms.push_back(temp);
	}

	for (int i = 0; i < enemyNo; ++i)
	{
		Enemy temp;
		temp.load(inFileEnemy);
		enemies.push_back(temp);
	}

	// Player
	Player player;
	if (inFilePlayerSaved)
	{
		player.loadPlayer(inFilePlayerSaved);
	}
	else
	{
		player.loadPlayer(inFilePlayer);
	}
	
	string input;

	// Parser
	Parser p;
	int lastRoom = -1;

	printIntro();

	srand(time(0));
	// Game loop
	while (!player.isDead() && (player.getCurrentRoom(rooms).getID() != 4))
	{
		if (lastRoom != player.getCurrentRoom(rooms).getID())
		{
			lastRoom = player.getCurrentRoom(rooms).getID();
			player.getCurrentRoom(rooms).print();
			printPlayerDetails(player);

			if (!player.isLoaded())
			{
				if (Random(1, 20) > 15)
				{
					hangmanPuzzle(player);
				}
				else if (Random(1, 10) > 4)
				{
					bool run = false;
					Enemy e = enemies.at(Random(0, enemies.size() - 1));
					cout << "You are attacked by a " << e.getName() << "!" << endl;

					while (!run && !e.isDead())
					{
						cout << "Fight or run?" << endl;

						getline(cin, input);

						if (input == "fight")
						{
							player.attack(e);

							if (!e.isDead())
							{
								e.attack(player);
							}
						}
						else if (input == "run")
						{
							e.attack(player);
							run = player.run(e);
						}
					}

					if (e.isDead())
					{
						player.getRewards(e);
					}
				}
				else
				{
					if (Random(1, 6) > 3)
					{
						cout << "The room is empty except for some gold which you take." << endl;
						player.addGold(Random(1, 100));
						printPlayerDetails(player);
					}
					else
					{
						cout << "The room is empty." << endl;
						printPlayerDetails(player);
					}
				}
			}
			player.setLoaded(false);
		}
		cout << "What would you like to do?" << endl;
		getline(cin, input);
		p.parse(input, player, rooms);
	}

	// End output
	if (player.isDead())
	{
		cout << "GAME OVER" << endl;
		cout << "You died! Not sure how..." << endl;
	}
	else
	{
		cout << "Congratulations!" << endl;
		cout << "You escaped the cave!" << endl;
		cout << "You win the game or something!" << endl;
	}

	system("pause");
	return 0;
}

void printPlayerDetails(Player& p)
{
	cout << "HP: " << p.getHP() << endl;
	cout << "Gold: " << p.getGold() << endl;
	cout << "XP: " << p.getXP() << endl;
	cout << "Attack Power: " << p.getAttack() << endl;
}
void printIntro()
{
	cout << "Welcome!" << endl;
	cout << "The following is a demonstration of a basic game engine." << endl;
	cout << "The game is played through a two word input system. For example\n";
	cout << "go east will move the player east if able. east or e may also be used." << endl;
	cout << "The common commands you will need are:" << endl;
	cout << "north, south, east, west, down, take [item], examine [room, item], use [item], combine [item], fight, save, run" << endl;
	cout << "Thank you for playing!" << endl;
	system("pause");
}

// https://moodle1314.dkit.ie/mod/resource/view.php?id=83944
void hangmanPuzzle(Player& p)
{
	// set-up
	const int MAX_WRONG = 8;  // maximum number of incorrect guesses allowed

	vector<string> words;  // collection of possible words to guess
	words.push_back("GUESS");
	words.push_back("HANGMAN");
	words.push_back("DIFFICULT");

	srand((unsigned int)time(0));
	random_shuffle(words.begin(), words.end());

	const string THE_WORD = words[0];         // word to guess
	int wrong = 0;                            // number of incorrect guesses
	string soFar(THE_WORD.size(), '-');       // word guessed so far
	string used = "";                         // letters already guessed

	cout << "A strange man appears before you and challenges you to guess the word in his head. He promises a reward if you succeed.\n";

	// main loop
	while ((wrong < MAX_WRONG) && (soFar != THE_WORD))
	{
		cout << "\n\nYou have " << (MAX_WRONG - wrong) << " incorrect guesses left.\n";
		cout << "\nYou've used the following letters:\n" << used << endl;
		cout << "\nSo far, the word is:\n" << soFar << endl;

		char guess;
		cout << "\n\nEnter your guess: ";
		cin >> guess;
		guess = toupper(guess); //make uppercase since secret word in uppercase

		while (used.find(guess) != string::npos)
		{
			cout << "\nYou've already guessed " << guess << endl;
			cout << "Enter your guess: ";
			cin >> guess;
			guess = toupper(guess);
		}

		used += guess;

		if (THE_WORD.find(guess) != string::npos)
		{
			cout << "That's right! " << guess << " is in the word.\n";

			// update soFar to include newly guessed letter
			for (int i = 0; i < THE_WORD.length(); ++i)
			if (THE_WORD[i] == guess)
				soFar[i] = guess;
		}
		else
		{
			cout << "Sorry, " << guess << " isn't in the word.\n";
			++wrong;
		}
	}

	// shut down
	if (wrong == MAX_WRONG)
	{
		cout << "\nYou didn't get it. The strange man disappears in a puff of smoke, but not before injuring you!" << endl;
		p.takeDamage(p.getHP() / 2);
	}
	else
	{
		cout << "\nYou guessed it! The strange man seemed pleased as he healed your wounds before vanishing." << endl;
		p.addHP(p.getHP() / 2);
	}
}