// Player.cpp - Player implementation

#include "Player.h"
#include "Item.h"
#include "Room.h"
#include "Random.h"
#include "Enemy.h"
#include <iostream>
#include <fstream>
#include <algorithm>
using std::ofstream;
using std::ifstream;
using std::string;
using std::endl;
using std::vector;
using std::cout;

// Combat stuffs
// Default constructor
Player::Player()
{

}

// Player constructor
Player::Player(const string& name, int hp, int atk)
{
	mName = name;
	mInventory.clear();
	mCurrentRoom = 0;
	mHP = hp;
	mGold = 0;
	mAttackPower = atk;
	mLoaded = false;
}

// Returns player's name
string Player::getName()
{
	return mName;
}

// Returns current Room for player
Room Player::getCurrentRoom(vector<Room>& rooms)
{
	return rooms[mCurrentRoom];
}

// Print's player's inventory of items
void Player::printInventory()
{
	cout << "----------------" << endl;
	cout << "Player Inventory" << endl;
	cout << "----------------" << endl;
	cout << "Gold: " << mGold << endl;
	for (int i = 0; i < mInventory.size(); ++i)
	{
		cout << mInventory[i].getName() << ": " << mInventory[i].getDescription() << endl;
	}
}

// Save player's details to file
void Player::savePlayer(ofstream& outFile)
{
	outFile << "Name= " << mName << endl;
	outFile << "mCurrentRoom= " << mCurrentRoom << endl;
	outFile << "HP= " << mHP << endl;
	outFile << "Gold= " << mGold << endl;
	outFile << "Attack= " << mAttackPower << endl;
	outFile << "Loaded= " << true << endl;
	outFile << "Items= " << mInventory.size() << endl;

	for (int i = 0; i < mInventory.size(); i++)
	{
		mInventory[i].save(outFile);
	}
}

// Load's player's details from file
void Player::loadPlayer(ifstream& inFile)
{
	string garbage;
	int items;

	inFile >> garbage >> mName;
	inFile >> garbage >> mCurrentRoom;
	inFile >> garbage >> mHP;
	inFile >> garbage >> mGold;
	inFile >> garbage >> mAttackPower;
	inFile >> garbage >> mLoaded;
	inFile >> garbage >> items;

	for (int i = 0; i < items; i++)
	{
		Item temp;
		temp.load(inFile);
		mInventory.push_back(temp);
	}
}

void Player::setLoaded(bool load)
{
	mLoaded = load;
}

// Add item to player's inventory
void Player::addItem(Item add)
{
	mInventory.push_back(add);
}

// Removes item from player's inventory by index
void Player::removeItem(int rem)
{
	mInventory.erase(mInventory.begin() + rem);
}

// Takes item from vector by name and adds it to player's inventory. Removes item from vector. Returns true if successful.
bool Player::takeItem(vector<Item>& items, const string& name)
{
	string itemName;

	for (int i = 0; i < items.size(); i++)
	{
		itemName = items[i].getName();
		transform(itemName.begin(), itemName.end(), itemName.begin(), ::tolower);
		if (itemName == name)
		{
			Item temp(items[i].getID(), items[i].getName(), items[i].getDescription());
			mInventory.push_back(temp);
			items.erase(items.begin() + i);
			return true;
		}
	}
	return false;
}

// Drops item from inventory by name and adds it to rooms's item list. Removes item from vector. Returns true if successful.
bool Player::dropItem(vector<Item>& items, const string& name)
{
	string itemName;

	for (int i = 0; i < mInventory.size(); i++)
	{
		itemName = mInventory[i].getName();
		transform(itemName.begin(), itemName.end(), itemName.begin(), ::tolower);
		if (itemName == name)
		{
			Item temp(mInventory[i].getID(), mInventory[i].getName(), mInventory[i].getDescription());
			items.push_back(temp);
			mInventory.erase(mInventory.begin() + i);
			return true;
		}
	}
	return false;
}

// Returns player's inventory
// Change to return reference
vector<Item> Player::getInventory()
{
	return mInventory;
}

// Subtracts damage from player's hp
void Player::takeDamage(int dmg)
{
	mHP -= dmg;
}

// Returns true if player's hp is 0 or less
bool Player::isDead()
{
	return mHP <= 0;
}

bool Player::removeItem(string& name)
{
	for (int i = 0; i < mInventory.size(); ++i)
	{
		if (mInventory[i].getName() == name)
		{
			mInventory.erase(mInventory.begin() + i);
			return true;
		}
	}
	return false;
}

// Combines 2 items into 1
bool Player::combineItems(Item i1, Item i2)
{
	if (i1.getName() == "Torch" || i2.getName() == "Torch")
	{
		if (i1.getName() == "Tinderbox" || i2.getName() == "Tinderbox")
		{
			Item i(0, "Lit Torch", "You didn't start the fire. Well, you kind of did.");
			mInventory.push_back(i);

			for (int i = 0; i < mInventory.size(); ++i)
			{
				if (mInventory[i].getName() == "Torch")
				{
					mInventory.erase(mInventory.begin() + i);
				}
			}
			return true;
		}
	}
	return false;
}

// Changes player's current location to one passed in
void Player::moveRoom(int nextID)
{
	mCurrentRoom = nextID;
}

// Checks if player has item in inventory by name
bool Player::hasItem(const string& item)
{
	for (int i = 0; i < mInventory.size(); ++i)
	{
		if (item == mInventory[i].getName())
		{
			return true;
		}
	}
	return false;
}

// Function to handle running from enemy
bool Player::run(Enemy& e)
{
	if (Random(1, 4) > 1)
	{
		cout << "You manage to escape." << endl;
		cout << "HP: " << getHP() << endl;
		cout << "Gold: " << getGold() << endl;
		cout << "XP: " << getXP() << endl;
		cout << "Attack Power: " << mAttackPower*2 << endl;
		return true;
	}
	else
	{
		cout << "You fail to escape." << endl;
		cout << "HP: " << getHP() << endl;
		cout << "Gold: " << getGold() << endl;
		cout << "XP: " << getXP() << endl;
		cout << "Attack Power: " << mAttackPower * 2 << endl;
		return false;
	}
}

// Increments player's gold value
void Player::addGold(int gold)
{
	mGold += gold;
}

// Returns if player was loaded from file
bool Player::isLoaded()
{
	return mLoaded;
}

// Returns player's max attack value
int Player::getAttack()
{
	return mAttackPower * 2;
}

// Returns player's xp amount
int Player::getXP()
{
	return mXP;
}

// Increments attack by amount passed in
void Player::addAttack(int atk)
{
	mAttackPower += atk;
}

// Returns gold of player
int Player::getGold()
{
	return mGold;
}

// Returns HP of player
int Player::getHP()
{
	return mHP;
}

// Function for attacking an enemy
void Player::attack(Enemy& e)
{
	int dmg = Random(1, mAttackPower) + mAttackPower;
	
	if (Random(1, 4) > 1)
	{
		e.takeDamage(dmg);
		cout << "You dealt " << dmg << " damage." << endl;
		if (e.isDead())
		{
			cout << "The " << e.getName() << " is dead." << endl;
		}
		else
		{
			cout << "The " << e.getName() << " has " << e.getHP() << " remaining." << endl;
		}
	}
	else
	{
		cout << "You missed." << endl;
	}
}

// Increments player's hp by amount passed in
void Player::addHP(int hp)
{
	mHP += hp;
}

// Adds the enemy's rewards to the player
void Player::getRewards(Enemy& e)
{
	mXP += e.getXP();
	mGold += e.getGold();
}