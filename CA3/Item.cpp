// Item.cpp - Item implementation

#include "Item.h"
#include <iostream>
#include <fstream>

using std::cout;
using std::endl;
using std::string;
using std::ofstream;
using std::ifstream;
using std::cin;
// Parent
// Constructors for item
Item::Item(int id, const string& name, const string& description)
{
	mID = id;
	mName = name;
	mDescription = description;
}

// Default constructor
Item::Item()
{
	mID = -1;
	mName = "N/A";
	mDescription = "N/A";
}

// Returns Item's name
string Item::getName()
{
	return this->mName;
}

// Returns Item's ID
int Item::getID()
{
	return mID;
}

// Returns Item's description
string Item::getDescription()
{
	return mDescription;
}

// Saves Item's details to file
void Item::save(ofstream& outFile)
{
	outFile << "ID= " << mID << endl;
	outFile << "ItemName= " << getName() << endl;
	outFile << "Description= " << mDescription << endl;
}

// Loads Item's details from file
void Item::load(ifstream& inFile)
{
	string garbage;
	inFile >> garbage >> mID;
	getline(inFile, garbage, ' ');
	getline(inFile, mName);
	getline(inFile, garbage, ' ');
	getline(inFile, mDescription);
}
// Prints outs the Item's description
void Item::displayDescription()
{
	cout << mDescription << endl;
}

// Print Item's name and description
void Item::print()
{
	cout << getName() << ": ";
	cout << mDescription << endl;
}