// Parser.cpp - Implementation of a parser to parse commands

#include "Parser.h"
#include "Player.h"
#include <sstream>
#include <vector>

// Testing
#include <iostream>
#include <algorithm>
#include <fstream>

using std::string;
using std::stringstream;
using std::set;
using std::cout;
using std::transform;
using std::vector;
using std::endl;
using std::cin;
using std::ofstream;

// Combat parsing
Parser::Parser()
{
	// Insertion of all commands in game

	// Moving
	commands.insert("go");
	commands.insert("north");
	commands.insert("south");
	commands.insert("east");
	commands.insert("west");
	commands.insert("up");
	commands.insert("down");
	commands.insert("n");
	commands.insert("s");
	commands.insert("e");
	commands.insert("w");
	commands.insert("u");
	commands.insert("d");
	commands.insert("travel");
	commands.insert("walk");
	commands.insert("run");
	commands.insert("stroll");
	commands.insert("crawl");
	commands.insert("enter");

	// Item-related
	commands.insert("take");
	commands.insert("examine");
	commands.insert("use");
	commands.insert("combine");
	commands.insert("look");
	commands.insert("drop");
	commands.insert("throw");
	commands.insert("pick");
	commands.insert("pickup");
	commands.insert("open");
	commands.insert("unlock");

	// Player-related
	commands.insert("i");
	commands.insert("inv");
	commands.insert("inventory");
	commands.insert("save");

	// Miscellaneous
	commands.insert("jump");
	commands.insert("eat");
}

// To lowercase code source:
// http://stackoverflow.com/questions/313970/stl-string-to-lower-case
// String stream as method of splitting string source:
// http://stackoverflow.com/questions/236129/how-to-split-a-string-in-c
void Parser::parse(const string& input, Player& p, vector<Room>& rooms)
{
	set<string>::iterator it;

	string firstWord;
	string secondWord;
	stringstream stream(input);
	
	transform(firstWord.begin(), firstWord.end(), firstWord.begin(), ::tolower);
	transform(secondWord.begin(), secondWord.end(), secondWord.begin(), ::tolower);

	// Seperate string into first and second words
	stream >> firstWord;
	stream >> secondWord;
	
	// If second word is these, ignore it and get next word
	if (secondWord == "the" || secondWord == "a" || secondWord == "that" || secondWord == "up" || secondWord == "at")
	{
		stream >> secondWord;
	}

	// Iterator = find firstword
	it = commands.find(firstWord);

	// Check if word not found. Else call appropriate parse function for input typed.
	if (it == commands.end())
	{
		cout << "I don't understand, please try again." << endl;
	}
	else if (*it == "north" || *it == "south" || *it == "east" || *it == "west" || *it == "up" || *it == "down" || 
			 *it == "n" || *it == "s" || *it == "e" || *it == "w" || *it == "u" || *it == "d")
	{
		parseMovement(*it, p, rooms);
	}
	else if (*it == "go" || *it == "walk" || *it == "travel" || *it == "run" || *it == "stroll" || *it == "crawl" ||
			 *it == "enter")
	{
		if (secondWord.empty())
		{
			cout << "Where would you like to " << *it << "?" << endl;
			cin >> secondWord;
			transform(secondWord.begin(), secondWord.end(), secondWord.begin(), ::tolower);
			// Empties cin
			cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		}

		parseMovement(secondWord, p, rooms);
	}
	else if (*it == "take" || *it == "examine" || *it == "use" || *it == "look" || *it == "drop" || *it == "pickup" ||
			 *it == "pick" || *it == "throw" || *it == "combine")
	{
		if (secondWord.empty())
		{
			cout << "What would you like to " << *it << "?" << endl;
			cin >> secondWord;
			transform(secondWord.begin(), secondWord.end(), secondWord.begin(), ::tolower);
			cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');
		}

		parseItemCommand(*it, secondWord, p, rooms);
		
	}
	else if (*it == "jump" || *it == "eat")
	{
		cout << "Now is not the time." << endl;
	}
	else if (*it == "inv" || *it == "inventory" || *it == "i")
	{
		p.printInventory();
	}
	else if (*it == "open" || *it == "unlock")
	{
		parseOpen(*it, secondWord, p, rooms);
	}
	else if (*it == "save")
	{
		parseSave(p, rooms);
	}

}

void Parser::parseSave(Player& p, vector<Room>& rooms)
{
	ofstream outFilePlayer("PlayerSave.txt");
	ofstream outFileRooms("RoomSave.txt");

	p.savePlayer(outFilePlayer);
	for (int i = 0; i < rooms.size(); ++i)
	{
		rooms[i].save(outFileRooms);
	}
}

// Parse input related to opening things
void Parser::parseOpen(const string& command, const string& item, Player& p, vector<Room>& rooms)
{
	int currentRoom = p.getCurrentRoom(rooms).getID();

	switch (currentRoom)
	{
	case 4:
		break;
	case 2:
		break;
	}
}

// Parse input related to moving rooms
void Parser::parseMovement(const string& move, Player& p, vector<Room>& rooms)
{
	Room currentRoom;
	int currentRoomNo;
	int nextRoom;
	currentRoomNo = p.getCurrentRoom(rooms).getID();
	currentRoom = p.getCurrentRoom(rooms);
	nextRoom = currentRoomNo;

	if (move == "north" || move == "n")
	{
		
		if (currentRoom.getExitNorth() == false)
		{
			cout << "You can't go that way." << endl;
			currentRoom.displayExits();
		}
		else
		{
			switch (currentRoomNo)
			{
			case 0:
				nextRoom = 1;
				break;
			case 1:
				nextRoom = 2;
				break;
			case 2:
				nextRoom = 3;
				break;
			case 3:
				nextRoom = 4;
				break;
			case 4:
				nextRoom = 5;
				break;
			}
			p.moveRoom(nextRoom);
		}
	}
	else if (move == "south" || move == "s")
	{
		
		if (currentRoom.getExitSouth())
		{
			cout << "You can't go that way." << endl;
			currentRoom.displayExits();
		}
		else
		{
			p.moveRoom(nextRoom);
		}
	}
	else if (move == "east" || move == "e")
	{
		currentRoom = p.getCurrentRoom(rooms);

		if (currentRoom.getExitEast() == false)
		{
			cout << "You can't go that way." << endl;
			currentRoom.displayExits();
		}
		else
		{
			p.moveRoom(nextRoom);
		}
	}
	else if (move == "west" || move == "w")
	{
		currentRoom = p.getCurrentRoom(rooms);

		if (currentRoom.getExitWest() == false)
		{
			cout << "You can't go that way." << endl;
			currentRoom.displayExits();
		}
		else
		{
			p.moveRoom(nextRoom);
		}
	}
	else if (move == "up" || move == "u")
	{
		currentRoom = p.getCurrentRoom(rooms);

		if (currentRoom.getExitUp() == false)
		{
			cout << "You can't go that way." << endl;
			currentRoom.displayExits();
		}
		else
		{
			p.moveRoom(nextRoom);
		}
	}
	else if (move == "down" || move == "d")
	{
		currentRoom = p.getCurrentRoom(rooms);

		if (currentRoom.getExitDown() == false)
		{
			cout << "You can't go that way." << endl;
			currentRoom.displayExits();
		}
		else
		{
			switch (currentRoom.getID())
			{
			case 0:
				nextRoom = 1;
				break;
			}
			p.moveRoom(nextRoom);
		}
	}
	else if (move == "cellar")
	{
		currentRoom = p.getCurrentRoom(rooms);

		if (currentRoom.getExitDown() == false)
		{
			cout << "You're already in the cellar. You're not getting any further down." << endl;
			currentRoom.displayExits();
		}
		else
		{
			switch (currentRoom.getID())
			{
			case 0:
				nextRoom = 1;
				break;
			}
			p.moveRoom(nextRoom);
		}
	}
}

// Parse input related to items
void Parser::parseItemCommand(const string& command, const string& item, Player& p, vector<Room>& rooms)
{
	Room currentRoom;
	currentRoom = p.getCurrentRoom(rooms);
	bool action;

	if (command == "take" || command == "pickup" || command == "pick")
	{
			action = p.takeItem(rooms[currentRoom.getID()].getItems(), item);
			if (action)
			{
				cout << "Taken." << endl;
				rooms[currentRoom.getID()].decrementNumItems();
			}
			else
			{
				cout << "I can't take that." << endl;
			}
	}
	else if (command == "drop" || command == "throw")
	{
		action = p.dropItem(rooms[currentRoom.getID()].getItems(), item);
		if (action)
		{
			cout << "Dropped." << endl;
			rooms[currentRoom.getID()].incrementNumItems();
		}
		else
		{
			cout << "You don't have that to drop." << endl;
		}
	}
	else if (command == "examine" || command == "look")
	{
		string currentRoom = p.getCurrentRoom(rooms).getName();
		transform(currentRoom.begin(), currentRoom.end(), currentRoom.begin(), ::tolower);
		if (item == "room" || item == "here" || item == currentRoom)
		{
			p.getCurrentRoom(rooms).print();
		}
		else
		{
			vector<Item> v = p.getCurrentRoom(rooms).getItems();
			string itemName;
			for (int i = 0; i < v.size(); ++i)
			{
				itemName = v[i].getName();
				transform(itemName.begin(), itemName.end(), itemName.begin(), ::tolower);
				if (item == itemName)
				{
					v[i].print();
				}
			}
		}
	}
	else if (command == "combine")
	{
		vector<Item> v = p.getInventory();
		string itemName;
		string useWith;
		int i1 = -1;
		int i2 = -1;

		for (int i = 0; i < v.size(); ++i)
		{
			itemName = v[i].getName();
			transform(itemName.begin(), itemName.end(), itemName.begin(), ::tolower);
			if (item == itemName)
			{
				i1 = i;
			}
		}

		cout << "Combine with what?" << endl;

		cin >> useWith;
		cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

		for (int i = 0; i < v.size(); ++i)
		{
			itemName = v[i].getName();
			transform(itemName.begin(), itemName.end(), itemName.begin(), ::tolower);
			if (useWith == itemName)
			{
				i2 = i;
			}
		}
		bool action = false;

		if (i1 != -1 && i2 != -1)
			action = p.combineItems(v[i1], v[i2]);
		else
			cout << "You don't have that." << endl;

		if (action)
		{
			cout << "Done." << endl;
		}
		else
		{
			cout << "You can't do that" << endl;
		}
	}
	else if (command == "use")
	{
		if (item == "potion")
		{
			string item = "Potion";

			if (p.hasItem("Potion"))
			{
				p.addHP(50);
				p.removeItem(item);
				cout << "You drink the potion and restore some HP." << endl;
			}
			else
			{
				cout << "You don't have that." << endl;
			}
		}
		else if (item == "poison")
		{
			string item = "Poison";

			if (p.hasItem("Poison"))
			{
				p.addAttack(50);
				p.removeItem(item);
				cout << "You increase the power of your weapon with the poison." << endl;
			}
			else
			{
				cout << "You don't have that." << endl;
			}
		}
	}
}

