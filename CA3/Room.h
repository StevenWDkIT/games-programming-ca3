// Room.h - Header file for room

#ifndef ROOM_H
#define ROOM_H

#include "Item.h"
#include <string>
#include <vector>
#include <fstream>

// Monster check
class Room
{
public:
	// Constructor
	Room(int id, int nItems, const std::string& name, const std::string& description, std::vector<Item>& items, bool mExitNorth, bool mExitSouth, bool mExitEast, bool mExitWest, bool mExitUp, bool mExitDown);
	Room();

	// Methods
	void displayDescription();
	void displayExits();
	void save(std::ofstream& outFile);
	void saveItems(std::ofstream& outFile);
	void load(std::ifstream& inFile);
	void loadItems(std::ifstream& inFile);
	void print();
	void printItems();
	void removeItem(const std::string& item);
	int getID();
	void decrementNumItems();
	void incrementNumItems();
	std::vector<Item>& getItems();

	bool getExitNorth();
	bool getExitSouth();
	bool getExitEast();
	bool getExitWest();
	bool getExitUp();
	bool getExitDown();

	std::string getName();

private:
	// Variables
	int mID;
	std::string mName;
	std::string mDescription;
	std::vector<Item> mItems;
	bool mExitNorth;
	bool mExitSouth;
	bool mExitEast;
	bool mExitWest;
	bool mExitUp;
	bool mExitDown;
	int mNumItems;
};

#endif