// Player.h - Header file for player

#ifndef PLAYER_H
#define PLAYER_H

#include "Enemy.h"
#include "Item.h"
#include "Room.h"
#include <vector>

class Enemy;

class Player
{
public:
	// Constructor
	Player();
	Player(const std::string& name, int hp, int atk);

	// Methods
	Room getCurrentRoom(std::vector<Room>& rooms);
	std::string getName();
	std::vector<Item> getInventory();
	
	void loadPlayer(std::ifstream& inFile);
	void savePlayer(std::ofstream& outFile);
	void moveRoom(int nextID);
	void addItem(Item add);
	void removeItem(int rem);
	void addGold(int gold);
	void printInventory();
	void takeDamage(int dmg);
	void attack(Enemy& e);
	void getRewards(Enemy& e);
	void setLoaded(bool load);
	void addHP(int hp);
	void addAttack(int atk);

	bool isLoaded();
	bool takeItem(std::vector<Item>& items, const std::string& name);
	bool dropItem(std::vector<Item>& items, const std::string& name);
	bool combineItems(Item i1, Item i2);
	bool hasItem(const std::string& item);
	bool isDead();
	bool removeItem(std::string& name);
	bool run(Enemy& e);

	int getGold();
	int getXP();
	int getHP();
	int getAttack();

private:
	// Variables
	std::vector<Item> mInventory;
	std::string mName;
	int mHP;
	int mAttackPower;
	int mXP;
	int mGold;
	int mCurrentRoom;
	bool mLoaded;
};


#endif