//Weapon.h - https://moodle1314.dkit.ie/mod/resource/view.php?id=87323

#ifndef WEAPON_H
#define WEAPON_H

#include "Range.h"
#include <string>

struct Weapon
{
	std::string Name;
	Range DamageRange;
};

#endif //WEAPON_H