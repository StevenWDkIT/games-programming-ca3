// Parser.h - Header file for the parser

#ifndef PARSER_H
#define PARSER_H

#include "Player.h"
#include <string>
#include <set>

// Implement parse combat
class Parser
{
public:
	Parser();
	void parse(const std::string& input, Player& p, std::vector<Room>& rooms);

private:
	std::set<std::string> commands;
	void parseItemCommand(const std::string& command, const std::string& item, Player& p, std::vector<Room>& rooms);
	void parseMovement(const std::string& move, Player& p, std::vector<Room>& rooms);
	void parseOpen(const std::string& command, const std::string& item, Player& p, std::vector<Room>& rooms);
	void Parser::parseSave(Player& p, std::vector<Room>& rooms);
};

#endif