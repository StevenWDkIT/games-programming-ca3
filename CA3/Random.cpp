//Random.cpp - https://moodle1314.dkit.ie/mod/resource/view.php?id=87323

#include "Random.h"
#include <cstdlib>

//Returns a random number in r
int Random(Range r)
{
	return r.mLow + rand() % ((r.mHigh + 1) - r.mLow);
}

//Returns a random number in [low, high]
int Random(int low, int high)
{
	return low + rand() % ((high + 1) - low);
}