// Item.h - Header file for item

#ifndef ITEM_H
#define ITEM_H

#include <string>

// Convert to Parent class
class Item
{
public:
	// Constructor
	Item();
	Item(int id, const std::string& name, const std::string& description);

	// Methods
	int getID();
	std::string getName();
	std::string getDescription();
	void save(std::ofstream& outFile);
	void load(std::ifstream& inFile);
	void displayDescription();
	void print();

private:
	// Variables
	int mID;
	std::string mName;
	std::string mDescription;
};

#endif