// Enemy.h - Header file for enemy

#ifndef ENEMY_H
#define ENEMY_H

#include "Player.h"
#include "Item.h"
#include "Room.h"
#include <vector>

class Player;

class Enemy
{
public:
	// Constructor
	Enemy();
	Enemy(const std::string& name, int hp, int atk, int xp);

	// Methods
	std::string getName();
	void load(std::ifstream& inFile);
	void loadWeapon(std::ifstream& inFile);
	void save(std::ofstream& outFile);
	void takeDamage(int dmg);
	bool isDead();
	int getHP();
	void attack(Player& p);
	int getXP();
	int getGold();

private:
	// Variables
	std::string mName;
	int mHP;
	int mAttackPower;
	int mXP;
	int mGold;
	int mCurrentRoom;
};


#endif