//Range.h - https://moodle1314.dkit.ie/mod/resource/view.php?id=87323

#ifndef RANGE_H
#define RANGE_H

//Defines a range [mLow, mHigh]
struct Range
{
	int mLow;
	int mHigh;
};
#endif //RANGE_H