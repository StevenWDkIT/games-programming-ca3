// Room.cpp - Room implementation

#include "Room.h"
#include <iostream>
#include <algorithm>

using std::transform;
using std::cout;
using std::endl;
using std::string;
using std::vector;
using std::ofstream;
using std::ifstream;

// Random stuff
// Constructor for room
Room::Room(int id, int nItems, const string& name, const string& description, vector<Item>& items, bool exitNorth, bool exitSouth, bool exitEast, bool exitWest, bool exitUp, bool exitDown)
{
	mID = id;
	mName = name;
	mDescription = description;
	mItems = items;
	mExitNorth = exitNorth;
	mExitSouth = exitSouth;
	mExitEast = exitEast;
	mExitWest = exitWest;
	mExitUp = exitUp;
	mExitDown = exitDown;
	mNumItems = nItems;
}

// Default constructor
Room::Room()
{
	mID = -1;
	mName = "default";
	mDescription = "default";
	mExitNorth = false;
	mExitSouth = false;
	mExitEast = false;
	mExitWest = false;
	mExitUp = false;
	mExitDown = false;
	mNumItems = -1;
}

// Returns Name of Room
string Room::getName()
{
	return mName;
}

// Returns ID of Room
int Room::getID()
{
	return mID;
}

// Returns reference to vector of items in Room
vector<Item>& Room::getItems()
{
	return mItems;
}

// Removes item by name from a room
void Room::removeItem(const string& item)
{
	string itemName;
	for (int i = 0; i < mItems.size(); i++)
	{
		itemName = mItems[i].getName();
		transform(itemName.begin(), itemName.end(), itemName.begin(), ::tolower);
		if (itemName == item)
		{
			mItems.erase(mItems.begin() + i);
		}
	}
}

// Returns if a Room has an exit north
bool Room::getExitNorth()
{
	return mExitNorth;
}

// Returns if a Room has an exit south
bool Room::getExitSouth()
{
	return mExitSouth;
}

// Returns if a Room has an exit east
bool Room::getExitEast()
{
	return mExitEast;
}

// Returns if a Room has an exit west
bool Room::getExitWest()
{
	return mExitWest;
}

// Returns if a Room has an exit up
bool Room::getExitUp()
{
	return mExitUp;
}

// Returns if a Room has an exit down
bool Room::getExitDown()
{
	return mExitDown;
}

// Prints out the room's description
void Room::displayDescription()
{
	cout << mDescription << endl;
}

// Prints out the possible exits of the room
void Room::displayExits()
{
	cout << "From this room, it seems you can go:" << endl;

	if (this->mExitNorth)
	{
		cout << "North" << endl;
	}

	if (this->mExitSouth)
	{
		cout << "South" << endl;
	}

	if (this->mExitEast)
	{
		cout << "East" << endl;
	}

	if (this->mExitWest)
	{
		cout << "West" << endl;
	}

	if (this->mExitUp)
	{
		cout << "Up" << endl;
	}

	if (this->mExitDown)
	{
		cout << "Down" << endl;
	}

	cout << endl;
}

// Saves Room's details to file
void Room::save(ofstream& outFile)
{
	outFile << "ID= " << mID << endl;
	outFile << "RoomName= " << getName() << endl;
	outFile << "RoomDesc= " << mDescription << endl;
	outFile << "ExitNorth= " << mExitNorth << endl;
	outFile << "ExitSouth= " << mExitSouth << endl;
	outFile << "ExitEast= " << mExitEast << endl;
	outFile << "ExitWest= " << mExitWest << endl;
	outFile << "ExitUp= " << mExitUp << endl;
	outFile << "ExitDown= " << mExitDown << endl;
	outFile << "NumberItems= " << mNumItems << endl;
	saveItems(outFile);
	outFile << endl;
}

// Saves Room's Items to file
void Room::saveItems(ofstream& outFile)
{
	for (vector<Item>::iterator it = this->mItems.begin(); it != this->mItems.end(); ++it)
	{
		(*it).save(outFile);
	}
}

// Loads Room from file
void Room::load(ifstream& inFile)
{
	string garbage;
	inFile >> garbage >> mID;
	getline(inFile, garbage, ' ');
	getline(inFile, mName);
	getline(inFile, garbage, ' ');
	getline(inFile, mDescription);
	inFile >> garbage >> mExitNorth;
	inFile >> garbage >> mExitSouth;
	inFile >> garbage >> mExitEast;
	inFile >> garbage >> mExitWest;
	inFile >> garbage >> mExitUp;
	inFile >> garbage >> mExitDown;
	inFile >> garbage >> mNumItems;
	if (mNumItems != 0)
	{
		loadItems(inFile);
	}
}

void Room::incrementNumItems()
{
	++mNumItems;
}

void Room::decrementNumItems()
{
	--mNumItems;
}
// Loads Room's Items
void Room::loadItems(ifstream& inFile)
{
// Load item's in from file and push into Room's vector
	Item temp;

	for (int i = 0; i < mNumItems; i++)
	{
		temp.load(inFile);
		mItems.push_back(temp);
	}
}

// Prints the room's name, description and list of items you can interact with
void Room::print()
{
	// Clears the console
	system("cls");
	cout << endl;
	cout << "--------------------------------------------------------------------------------" << endl;
	cout << "\t\t\t\t\t" << mName << endl;
	cout << "--------------------------------------------------------------------------------" << endl;
	cout << mDescription << "\n" << endl;
	printItems();
}

// Prints the items in the room you can see
void Room::printItems()
{
	cout << endl;
	if (mItems.size() != 0)
	{
		cout << "--------------------------------------------------------------------------------" << endl;
		cout << "You can see:" << endl;
		for (int i = 0; i < mItems.size(); ++i)
		{
			cout << mItems[i].getName() << endl;
		}
	}
	cout << "--------------------------------------------------------------------------------" << endl;

}