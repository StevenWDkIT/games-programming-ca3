// Enemy.cpp - Enemy implementation

#include "Enemy.h"
#include "Item.h"
#include "Room.h"
#include "Random.h"
#include <iostream>
#include <fstream>
#include <algorithm>
using std::ofstream;
using std::ifstream;
using std::string;
using std::endl;
using std::vector;
using std::cout;

// Combat stuffs
// Default constructor
Enemy::Enemy()
{

}

// Player constructor
Enemy::Enemy(const string& name, int hp, int atk, int xp)
{
	mName = name;
	mCurrentRoom = 0;
	mHP = hp;

	mXP = xp;
	mAttackPower = atk;
}

// Returns player's name
string Enemy::getName()
{
	return mName;
}

// Subtracts damage from player's hp
void Enemy::takeDamage(int dmg)
{
	mHP -= dmg;
}

// Returns true if player's hp is 0 or less
bool Enemy::isDead()
{
	return mHP <= 0;
}

void Enemy::attack(Player& p)
{
	if (Random(1, 10) % 2 == 0)
	{
		int dmg = Random(1, mAttackPower);
		p.takeDamage(dmg);
		cout << "The " << mName << " dealt " << dmg << " damage to you." << endl;
	}
	else
	{
		cout << "The " << mName << " missed." << endl;
	}
}

int Enemy::getXP()
{
	return mXP;
}

int Enemy::getGold()
{
	return mGold;
}

int Enemy::getHP()
{
	return mHP;
}

// Saves Room's details to file
void Enemy::save(ofstream& outFile)
{
	outFile << "Name= " << mName << endl;
	outFile << "HP= " << mHP << endl;
	outFile << "Atk= " << mAttackPower << endl;
	outFile << "XP= " << mXP << endl;
	outFile << endl;
}

// Loads Enemy from file
void Enemy::load(ifstream& inFile)
{
	string garbage;
	getline(inFile, garbage, ' ');
	getline(inFile, mName);
	inFile >> garbage >> mHP;
	inFile >> garbage >> mAttackPower;
	inFile >> garbage >> mXP;
	mGold = Random(1, 100) * Random(1, mAttackPower);
}